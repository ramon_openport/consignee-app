import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpodComponent } from './epod.component';

describe('EpodComponent', () => {
  let component: EpodComponent;
  let fixture: ComponentFixture<EpodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
