import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { OtpComponent } from './components/otp/otp.component';
import { ForgotComponent } from './components/forgot/forgot.component';
import { FeedComponent } from './components/feed/feed.component';
import { TntComponent } from './components/tnt/tnt.component';
import { DetailsComponent } from './components/details/details.component';
import { EpodComponent } from './components/epod/epod.component';
import { SkuComponent } from './components/sku/sku.component';
import { SignatureComponent } from './components/signature/signature.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'otp', component: OtpComponent },
  { path: 'forgot', component: ForgotComponent },
  { path: '', component: FeedComponent },
  { path: 'tnt', component: TntComponent },
  { path: 'details', component: DetailsComponent },
  { path: 'epod', component: EpodComponent },
  { path: 'sku', component: SkuComponent },
  { path: 'signature', component: SignatureComponent },
  { path: '**', redirectTo: 'feed' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  //imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
