import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatInputModule
} from '@angular/material';

import { FeedComponent } from './components/feed/feed.component';
import { TntComponent } from './components/tnt/tnt.component';
import { DetailsComponent } from './components/details/details.component';
import { EpodComponent } from './components/epod/epod.component';
import { SkuComponent } from './components/sku/sku.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { OtpComponent } from './components/otp/otp.component';
import { SignatureComponent } from './components/signature/signature.component';
import { ForgotComponent } from './components/forgot/forgot.component';

@NgModule({
  declarations: [
    AppComponent,
    FeedComponent,
    TntComponent,
    DetailsComponent,
    EpodComponent,
    SkuComponent,
    RegisterComponent,
    LoginComponent,
    OtpComponent,
    SignatureComponent,
    ForgotComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
